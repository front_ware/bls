//Include required modules
var gulp = require("gulp")
var babelify = require('babelify')
var browserify = require("browserify")
var source = require("vinyl-source-stream");
var ngAnnotate   = require('browserify-ngannotate');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');


//Convert ES6 ode in all js files in src/js folder and copy to 
//build folder as bundle.js
gulp.task("build", function () {
    return browserify({
        entries: ["./static/js/main.js"],
        debug: false,
        cache: {},
        packageCache: {},
        fullPaths: false
    })
        .transform('babelify', {presets: ['es2015']})
        .transform(ngAnnotate)
        .bundle()
        .pipe(source("main.js"))
        .pipe(gulp.dest("./output/build"));
});

gulp.task('sass', function () {
    gulp.src('./static/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3', 'Firefox 14']
        }))
        //.pipe(minifyCss())
        .pipe(gulp.dest('./_css'));
});

gulp.task('concat', function () {
    gulp.src('./_css/**/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./output/build'))
});

gulp.task('watch', function() {
    gulp.watch('./static/js/**/*.js', ['build']);
    gulp.watch('./static/scss/**/*.scss', ['sass']);
    gulp.watch('./_css/**/*.css', ['concat']);
});
