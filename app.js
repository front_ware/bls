var express = require('express');
var app = express();


app.use(express.static('static'));
app.use(express.static('output'));


app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.all('/', function (req, res) {
    res.render('view.html');
});

app.all('/partials/*', function (req, res) {
    res.render(req.url.substr(1));
});

app.all('*', function (req, res) {
    res.render('view.html');
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});