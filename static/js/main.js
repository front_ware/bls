require('babel-polyfill');
require('angular-scroll');
require('angular-route');
require('angular-loading-bar');

const angular = require('angular');

require('./app/app.module');
