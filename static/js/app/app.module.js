angular.module('BlsApp', ['duScroll', 'ngRoute', 'angular-loading-bar',
    require('./common/common.module'),
    require('./search/search.module'),
    require('./post/post.module')
])
    .config(require('./common/router.config.js'))
    .config(require('./common/cfpLoadingBarProvider.config'));
