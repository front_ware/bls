module.exports = ($scope, wordpressApi, $document, $compile, sessionStorage) => {
    'ngInject';

    let $searchResults = angular.element(document.querySelector('.molecule-search-results'));

    let handleResults = (data) => {
        $scope.disabled = false;
        $scope.searchResults = data.data.posts;
        $scope.searchedSite = $scope.formData.site;
        $document.scrollToElementAnimated($searchResults, 0, 500);
        sessionStorage.setItem('formState', $scope.formData);
        sessionStorage.setItem('searchResults', data.data.posts);
    };
    let onError = (data) => {
        $scope.disabled = false;
        sessionStorage.removeItem('formState');
        sessionStorage.removeItem('searchResults');
        $scope.searchResults = [];
        handleModal(true, 'Oops an error occured', data.data.message);
    };
    let init = () => {
        $scope.formData = sessionStorage.getItem('formState') ? JSON.parse(sessionStorage.getItem('formState')) : {};
        $scope.searchResults = sessionStorage.getItem('searchResults') ? JSON.parse(sessionStorage.getItem('searchResults')) : [];
        $scope.searchedSite = $scope.formData.site ? $scope.formData.site : '';
        $scope.disabled = false;
        $scope.modal = {};
    };
    let handleModal = (open, title, message) => {
        $scope.modal = $scope.modal || {};

        if (title) {
            $scope.modal.title = title;
        }
        if (message) {
            $scope.modal.message = message;
        }

        $scope.modal.open = open;
    };
    $scope.validate = (data, type) => {
        let messages = [];

        switch (type) {
            case 'site':
                if (!data.length) {
                    messages.push('site is required');
                } else {
                    if (!data.match(/^(https?:\/\/)?[\w,\-]+\.[a-z]{2,}/)) {
                        messages.push('site address is not correct');
                    }

                    if (data.length > 100) {
                        messages.push('site is too long');
                    }
                }

                break;
            case 'search':
                if (data && data.length > 100) {
                    messages.push('search is too long');
                }
                break;
            case 'order_by':
                if (data !== 'date' && data !== 'modified' && data !== 'title') {
                    messages.push('order by has unknown value');
                }
                break;
            case 'number':
                if (!data.toString().length) {
                    messages.push('number is required');
                } else {
                    if (data > 100) {
                        messages.push('number is too big');
                    } else if (data < 5) {
                        messages.push('number is too small');
                    }
                }
                break;
        }

        return {
            valid: messages.length === 0,
            messages
        };
    };

    $scope.formValid = () => {
        let messages = [
            ...$scope.validate($scope.formData.site, 'site').messages,
            ...$scope.validate($scope.formData.search, 'search').messages,
            ...$scope.validate($scope.formData.number, 'number').messages,
            ...$scope.validate($scope.formData.order_by, 'order_by').messages
        ];

        return {
            valid: messages.length === 0,
            messages
        };
    };


    $scope.submitForm = (isValid) => {
        if (isValid) {
            let valid = $scope.formValid();
            if (valid) {
                $scope.disabled = true;
                wordpressApi.getPosts($scope.formData).then(handleResults, onError);
            } else {
                handleModal(true, 'Oops an error occured', valid.messages.join('<br/>'));
            }
        }
    };

    init();
};
