angular.module('BlsApp-Search', [])
    .factory('wordpressApi', require('./wordpressApi.service'))
    .controller('searchPageCtrl', require('./searchPageCtrl.controller'));

module.exports = 'BlsApp-Search';
