module.exports = ($http) => {
    'ngInject';

    const getPosts = (params) => {
        let apiUrl = `https://public-api.wordpress.com/rest/v1.1/sites/${encodeURIComponent(params.site)}/posts/`;

        return $http({
            url: apiUrl,
            method: 'GET',
            params
        });
    };

    const getPost = (params) => {
        let apiUrl = `https://public-api.wordpress.com/rest/v1.1/sites/${params.site}/posts/${params.id}`;

        return $http({
            url: apiUrl,
            method: 'GET'
        });
    };

    return {
        getPosts,
        getPost
    }
};
