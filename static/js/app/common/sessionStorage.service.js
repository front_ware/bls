module.exports = ($window) => {
    'ngInject';

    const setItem = (name,value) => {
        $window.sessionStorage.setItem(name, JSON.stringify(value));
    };

    const getItem = (name) => {
        return $window.sessionStorage.getItem(name);
    };

    const removeItem = (name) => {
        $window.sessionStorage.removeItem(name);
    };

    return {
        setItem,
        getItem,
        removeItem
    }
};
