module.exports = function () {
    'ngInject';

    return function (string) {
        return string.replace(/%252F/gi, '%2F');
    }
};
