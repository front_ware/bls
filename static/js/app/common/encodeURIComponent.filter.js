module.exports = function () {
    'ngInject';

    return function (string) {
        return encodeURIComponent(string);
    }
};
