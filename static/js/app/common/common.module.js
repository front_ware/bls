angular.module('BlsApp-Commons', [])
    .filter('html', require('./html.filter'))
    .filter('encodeURIComponent', require('./encodeURIComponent.filter'))
    .filter('encodeURIComponentSlash', require('./encodeURIComponentSlash.filter'))
    .filter('decodeSlash', require('./decodeSlash.filter'))
    .factory('sessionStorage', require('./sessionStorage.service'));

module.exports = 'BlsApp-Commons';
