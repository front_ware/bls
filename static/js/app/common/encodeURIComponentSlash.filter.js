module.exports = function () {
    'ngInject';

    return function (string) {
        return encodeURIComponent(string).replace(/%2F/gi, '%252F');
    }
};
