module.exports = ($routeProvider, $locationProvider) => {
    'ngInject';

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/', {
            templateUrl: '/partials/search.html',
            controller: 'searchPageCtrl'
        })
        .when('/post/:site/:id', {
            templateUrl: '/partials/post.html',
            controller: 'postPageCtrl'
        })

        .otherwise({
            redirectTo: '/'
        });
};

