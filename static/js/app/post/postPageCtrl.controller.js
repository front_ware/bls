module.exports = ($scope, $routeParams, wordpressApi, $window, $filter) => {
    'ngInject';

    let params = $routeParams;

    params.site = $filter('decodeSlash')(params.site);

    $scope.back = () => {
        $window.history.back();
    };

    wordpressApi.getPost(params).then((data) => {
        $scope.singlePost = data.data;
        console.log('success', data);
    }, (data) => {
        console.log('errorer', data);
    });
};

