describe('wordpressApi', () => {
    let wordpressApi;
    let $scope;
    let $httpBackend;

    beforeEach(module('BlsApp'));

    beforeEach(inject((_wordpressApi_) => {
        wordpressApi = _wordpressApi_;
    }));

    beforeEach(inject((_$httpBackend_) => {
        $httpBackend = _$httpBackend_;

        $httpBackend.when('GET', /^https:\/\/public-api\.wordpress\.com.*/)
            .respond({found: 5});
    }));

    beforeEach(inject(($rootScope) => {
        $scope = $rootScope;
    }));

    it('can get an instance of my factory', () => {
        let params = {
            site: 'en.blog.wordpress.com',
            search: '',
            number: 5,
            order_by: 'date'
        };

        var promise = wordpressApi.getPosts(params);

        promise.then((data) => {
            expect(data.found).toBe(5);
            //expect(true).toBe(false);
        }, (data) => {
            expect(true).toBe(false); //just for to get an error
        });

        $scope.$digest();
    });
});

