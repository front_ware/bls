describe('searchPage', () => {

    beforeEach(module('BlsApp'));

    let $controller;
    let controller;
    let $scope = {};

    beforeEach(inject((_$controller_) => {
        $controller = _$controller_;
        controller = $controller('searchPageCtrl', {$scope: $scope});
    }));

    describe('Form validation', () => {

        it('test site validation', () => {
            let type = 'site';

            //no valid
            expect($scope.validate('', type).valid)
                .toBe(false);
            expect($scope.validate('teeest', type).valid)
                .toBe(false);
            expect($scope.validate('teeest.d', type).valid)
                .toBe(false);
            expect($scope.validate('teeest.123', type).valid)
                .toBe(false);
            expect($scope.validate('ftp://teeest.com', type).valid)
                .toBe(false);
            expect($scope.validate('http:/teeest.pl', type).valid)
                .toBe(false);
            expect($scope.validate('http://teeest', type).valid)
                .toBe(false);
            expect($scope.validate('teeest', type).valid)
                .toBe(false);
            expect($scope.validate('dasdadsadasdadsada.dsadasddasdadsadasdaddasdadsadasdadsada.dsadasddasdadsadasdaddasdadsadasdadsada.dsadasddasdadsadasdaddasdadsadasd', type).valid)
                .toBe(false);

            //valid
            expect($scope.validate('teeest.pl', type).valid)
                .toBe(true);
            expect($scope.validate('http://en.blog.wordpress.com', type).valid)
                .toBe(true);
            expect($scope.validate('dlaczegoplacze.com', type).valid)
                .toBe(true);
            expect($scope.validate('moze-tak123.com', type).valid)
                .toBe(true);
        });

        it('test search validation', function () {
            let type = 'search';

            //no valid
            expect($scope.validate('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper blandit felis, in lobortis nibh ultrices eu. Ut convallis mauris vitae est auctor congue.', type).valid)
                .toBe(false);

            //valid
            expect($scope.validate('lorem', type).valid)
                .toBe(true);
            expect($scope.validate('', type).valid)
                .toBe(true);
        });

        it('test number validation', function () {
            let type = 'number';

            //no valid
            expect($scope.validate(0, type).valid)
                .toBe(false);
            expect($scope.validate(4, type).valid)
                .toBe(false);
            expect($scope.validate('', type).valid)
                .toBe(false);
            expect($scope.validate(101, type).valid)
                .toBe(false);

            //valid
            expect($scope.validate(5, type).valid)
                .toBe(true);
            expect($scope.validate(15, type).valid)
                .toBe(true);
            expect($scope.validate(100, type).valid)
                .toBe(true);
        });

        it('test search validation', function () {
            let type = 'order_by';

            //no valid
            expect($scope.validate('', type).valid)
                .toBe(false);
            expect($scope.validate('time', type).valid)
                .toBe(false);

            //valid
            expect($scope.validate('modified', type).valid)
                .toBe(true);
        });

        it('test search validation', function () {
            let type = 'order_by';

            //no valid
            expect($scope.validate('', type).valid)
                .toBe(false);
            expect($scope.validate('time', type).valid)
                .toBe(false);

            //valid
            expect($scope.validate('modified', type).valid)
                .toBe(true);
        });
    });

});